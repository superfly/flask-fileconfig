import pytest
from flask import Flask


@pytest.fixture
def app(request):
    app = Flask(request.module.__name__)
    app.testing = True
    return app
