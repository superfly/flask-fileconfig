from flask_fileconfig import FileConfig, IniConfig, JsonConfig, YamlConfig


def test_fileconfig_without_filename(app):
    FileConfig(app)
    assert not isinstance(app.config, (IniConfig, JsonConfig, YamlConfig))
