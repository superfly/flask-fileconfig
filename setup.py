import io
import re
from setuptools import setup


with io.open('README.rst', 'rt', encoding='utf8') as readme_file:
    README = readme_file.read()


with io.open('flask_fileconfig/__init__.py', 'rt', encoding='utf8') as version_file:
    VERSION = re.search(r'__version__ = \'(.*?)\'', version_file.read(), re.M).group(1)


setup(
    name='Flask-FileConfig',
    version=VERSION,
    url='https://gitlab.com/superfly/flask-fileconfig',
    license='MIT',
    author='Raoul Snyman',
    author_email='raoul@snyman.info',
    description='Adds loading configuration from .conf or .ini files to Flask',
    long_description=README,
    packages=['flask_fileconfig'],
    include_package_data=True,
    python_requires='>= 3.5',
    install_requires=['Flask>=0.10', 'pyyaml'],
)
