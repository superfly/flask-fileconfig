Flask-FileConfig
================

Flask-FileConfig is an extension for `Flask`_ that adds support for loading configuration from ``.conf`` or ``.ini``
files.

Installing
----------

.. note::

   Flask-FileConfig is not yet up on PyPI.

Install Flask-FileConfig using `pip`_:

.. code-block:: text

   $ pip install Flask-FileConfig


Simple Example
--------------

.. code-block:: python

   from flask import Flask
   from flask_fileconfig import FileConfig

   app = Flask(__name__)
   FileConfig(app, 'myapp.conf')


Links
-----

- Code: https://gitlab.com/superfly/flask-fileconfig


.. _Flask: https://palletsprojects.com/p/flask/
.. _pip: https://pip.pypa.io/en/stable/quickstart/
