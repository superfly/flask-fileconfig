import logging
import os

from flask.config import Config
from yaml import load
try:
    from yaml import CLoader as Loader
except ImportError:
    from yaml import Loader

log = logging.getLogger(__name__)


class YamlConfig(Config):
    def __init__(self, filename=None, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.filename = filename
        if self.filename:
            self.load_from_file()

    def load_from_file(self, filename=None):
        """
        Load the Flask configuration from a config file
        """
        if not filename and not self.filename:
            log.warning('No filename supplied')
            return
        elif not filename:
            filename = self.filename
        if not os.path.exists(filename):
            log.warning('{} does not exist'.format(filename))
            return
        with open(filename) as yaml_file:
            flask_config = load(yaml_file.read(), Loader=Loader)
        self.update(flask_config)
