import json
import logging
import os
from configparser import ConfigParser

from flask.config import Config

BOOLEAN_VALUES = ['yes', 'true', 'on', 'no', 'false', 'off']

log = logging.getLogger(__name__)


class IniConfig(Config):
    def __init__(self, filename=None, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.filename = filename
        if self.filename:
            self.load_from_file()

    def load_from_file(self, filename=None):
        """
        Load the Flask configuration from a config file
        """
        if not filename and not self.filename:
            log.warning('No filename supplied')
            return
        elif not filename:
            filename = self.filename
        if not os.path.exists(filename):
            log.warning('{} does not exist'.format(filename))
            return
        flask_config = {}
        config = ConfigParser()
        config.read(filename)
        for section in config.sections():
            for option in config.options(section):
                # Get the value, skip it if it is blank
                string_value = config.get(section, option)
                if not string_value:
                    continue
                # Try to figure out what type it is
                if string_value.isnumeric() and '.' in string_value:
                    value = config.getfloat(section, option)
                elif string_value.isnumeric():
                    value = config.getint(section, option)
                elif string_value.lower() in BOOLEAN_VALUES:
                    value = config.getboolean(section, option)
                elif string_value.startswith('{'):
                    # Try to load string values beginning with '{' as JSON
                    try:
                        value = json.loads(string_value)
                    except ValueError:
                        # If this is not JSON, just use the string
                        value = string_value
                else:
                    value = string_value
                # Set up the configuration key
                if section == 'flask':
                    # Options in the flask section don't need FLASK_*
                    key = option.upper()
                else:
                    key = '{}_{}'.format(section, option).upper()
                # Save this into our flask config dictionary
                flask_config[key] = value
        self.update(flask_config)
