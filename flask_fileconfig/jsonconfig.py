import logging
import os

from flask.config import Config
from json import loads

log = logging.getLogger(__name__)


class JsonConfig(Config):
    def __init__(self, filename=None, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.filename = filename
        if self.filename:
            self.load_from_file()

    def load_from_file(self, filename=None):
        """
        Load the Flask configuration from a config file
        """
        if not filename and not self.filename:
            log.warning('No filename supplied')
            return
        elif not filename:
            filename = self.filename
        if not os.path.exists(filename):
            log.warning('{} does not exist'.format(filename))
            return
        with open(filename) as json_file:
            flask_config = loads(json_file.read())
        self.update(flask_config)
