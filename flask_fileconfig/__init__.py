from pathlib import Path

from flask_fileconfig.iniconfig import IniConfig
from flask_fileconfig.jsonconfig import JsonConfig
from flask_fileconfig.yamlconfig import YamlConfig

__version__ = '0.1'


class FileConfigError(Exception):
    pass


class FileConfig(object):
    def __init__(self, app=None, filename=None):
        if app:
            self.init_app(app, filename)

    def init_app(self, app, filename=None):
        if filename and Path(filename).exists:
            filename = Path(filename)
            if filename.suffix.lower() in ['.ini', '.conf']:
                ConfigClass = IniConfig
            elif filename.suffix.lower() in ['.yml', '.yaml']:
                ConfigClass = YamlConfig
            elif filename.suffix.lower() in ['.json']:
                ConfigClass = JsonConfig
            else:
                raise FileConfigError('Unable to process file with extension {}'.format(filename.suffix))
            app.config = ConfigClass(filename, root_path=app.root_path, defaults=app.config)
